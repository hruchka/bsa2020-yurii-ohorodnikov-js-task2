//"npx wdio wdio.conf.js" to run the test
const assert = require('assert');

describe('Pop the Bubbles Game', () => {
    xit('should pop the bubbles constantly', () => {
        browser.url('https://task1-bvckdxdkxw.now.sh/');
        let bubble = $('.bubble');
        let score = $('#score');
        let count = 0;
        while(bubble){
            bubble.waitForClickable({ timeout: 5000 });
            bubble.click();
            count += 1;
            assert.equal(score.getText(),count);
        }
    })
    xit('should pop the bubbles that appeared in 5sec', () => {
        browser.url('https://task1-bvckdxdkxw.now.sh/')
        browser.pause(5000);
        let bubbles = $$('.bubble');
        let score = $('#score');
        const bubblesCount = Array.from(bubbles).length;
        Array.from(bubbles).forEach(b => {b.click();});
        assert.equal(Number(score.getText()), bubblesCount);
    })
})
