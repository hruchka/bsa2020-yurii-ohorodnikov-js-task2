function superSort(str) {
  return str.split(' ').sort((a, b) => a.replace(/[0-9]/g, '') > b.replace(/[0-9]/g, '') ? 1 : -1)
}
console.log(superSort('mic09ha1el 4b5en6 michelle be4atr3ice'));