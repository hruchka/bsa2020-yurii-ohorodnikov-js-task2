function getDaysForMonth(month){
    let amountOfDays;
    switch(month){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            amountOfDays = 31;
            break; 
        case 2:
            amountOfDays = 28;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            amountOfDays = 30;
            break;
        default:
            console.log(`You have entered the wrong value: ${month}.`);
    }
    return amountOfDays;
}

console.log(getDaysForMonth(0))